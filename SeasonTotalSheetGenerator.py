from helper_functions.SQLHelpers import get_all_g_league_play_records_for_season
import pandas as pd


def generate_new_sheet_from_g_league_table_data(season, mssql_engine):
    g_league_records_df = get_all_g_league_play_records_for_season(mssql_engine, season)
    g_league_records_summed = g_league_records_df.groupby(['Play_Series', 'Play_Call'])[["Possessions", "Points_Scored", "Shooting_Fouls", "Turnovers", "Assists"]].sum().reset_index()

    play_keywords = ('Transition', 'Auto Series', '2 Up Series', 'Open', 'Drag', 'Playground', 'Random P/R', '34', 'SOB'
                     , '1-4 Flat', 'Zone', 'Blur', '2nd Ballscreen', 'BOB', '1', 'EoQ', 'Basic', 'Elbow'
                     , 'Pistol action', 'Rub', 'Pitch game', 'Post Up')

    writer = pd.ExcelWriter('NAZ_total_season_play_stats.xlsx', engine='xlsxwriter')

    workbook = writer.book
    worksheet = workbook.add_worksheet('Sheet1')

    first_column_format = workbook.add_format({'bold': True, 'bottom':5, 'top':5, 'right':5})
    header_format = workbook.add_format({'bold': True, 'bottom':5})
    standard_format = workbook.add_format({'bottom':5, 'top':5})
    keyword_format = workbook.add_format({'bold': True, 'underline': True, 'bottom':5, 'top':5})

    worksheet.write(0, 0, "Play Series")
    worksheet.write(0, 1, "Play Call", header_format)
    worksheet.write(0, 2, "Possessions", header_format)
    worksheet.write(0, 3, "Points Scored", header_format)
    worksheet.write(0, 4, "Shooting Fouls", header_format)
    worksheet.write(0, 5, "Turnovers", header_format)
    worksheet.write(0, 6, "Assists", header_format)

    underline_control_flag = 0
    for i, row in enumerate(g_league_records_summed.values):
        underline_control_flag = 0
        for j, val in enumerate(row):
            worksheet.set_column(i, j, 15)
            if j == 1 and val in play_keywords:
                underline_control_flag = 1
            if underline_control_flag == 1:
                worksheet.write(i+1, j, val, keyword_format)
            elif j == 0:
                worksheet.write(i+1, j, val, first_column_format)
            else: worksheet.write(i+1, j, val, standard_format)

    writer.save()