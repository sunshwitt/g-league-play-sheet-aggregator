import pandas as pd
import sqlalchemy
import sqlalchemy.sql


def get_g_league_game_code_by_opponent_and_game_date(mssql_engine, game_date, season):
    sql_stmt = '''
    select distinct game_id from
      (select game_id, convert(datetime, game_date) as game_date, home_team_abbr as opp_name from dbo.nba_game_info
      where season = :szn
      and league_abbr = 'NBDL'
      and (visitor_team_abbr = 'NAS')
      union
      select game_id, convert(datetime, game_date) as game_date, visitor_team_abbr as opp_name from dbo.nba_game_info
      where season = :szn
      and league_abbr = 'NBDL'
      and (home_team_abbr = 'NAS')) naz_games
            where game_date = :gameDate
    '''
    sql_stmt_populated = sqlalchemy.sql.text(sql_stmt)
    with mssql_engine.connect() as conn:
        game_code = \
            pd.read_sql(sql_stmt_populated, conn,
                        params={'szn': season, 'gameDate': game_date})[
                'game_id'][0]
        return game_code


def delete_g_league_play_records_by_game_code(mssql_engine, game_code):
    sql_stmt = '''
    delete from etl.g_league_play_call_outcomes where game_code = :gc
    '''
    sql_stmt_populated = sqlalchemy.sql.text(sql_stmt)
    with mssql_engine.connect() as conn:
        conn.execute(sql_stmt_populated, gc=game_code)


def get_all_g_league_play_records_for_season(mssql_engine, season):
    sql_stmt = '''
    select * from etl.g_league_play_call_outcomes
        where game_code in (select distinct game_id 
                        from dbo.nba_game_info
                        where season = :szn)
    '''

    sql_stmt_populated = sqlalchemy.sql.text(sql_stmt)
    with mssql_engine.connect() as conn:
        g_league_records_df = pd.read_sql(sql_stmt_populated, conn, params={'szn': season})
        return g_league_records_df
