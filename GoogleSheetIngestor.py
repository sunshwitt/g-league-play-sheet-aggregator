import pandas as pd
import numpy as np
import sqlalchemy
import datetime
from helper_functions.SQLHelpers import get_g_league_game_code_by_opponent_and_game_date, \
    delete_g_league_play_records_by_game_code


def ingest_google_sheet_by_name(sheet_name, table_name, season, mssql_engine):
    data = pd.read_excel(sheet_name, sheet_name="Sheet1")
    data.rename(
        columns=lambda x: x.replace("%", "Percentage").replace("&", "and"), inplace=True)
    data_df = data.rename(columns={'Play Series': 'Play_Series', 'Play Call': 'Play_Call',
                                   'Points Scored ': 'Points_Scored', 'Shooting Fouls': 'Shooting_Fouls',
                                   'Conversion Percentage (Off. Efficiency)': 'ConversionPercentage'})
    game_date = str(data_df['Date'][0])
    game_code = get_g_league_game_code_by_opponent_and_game_date(mssql_engine, game_date, season)

    delete_g_league_play_records_by_game_code(mssql_engine, game_code)
    data_df.loc[:, 'game_code'] = game_code

    data_df.to_sql(table_name, mssql_engine, schema='etl', if_exists='append', index=False)
