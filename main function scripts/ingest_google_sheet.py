from GoogleSheetIngestor import ingest_google_sheet_by_name
from SeasonTotalSheetGenerator import generate_new_sheet_from_g_league_table_data
import sqlalchemy
import sqlalchemy.sql


def main():
    sheet_path = 'C:/Users/hwitt/Documents/GLeaguePlayStats/vs GBO 1-5-20.xlsx'
    table_name = 'g_league_play_call_outcomes'
    season = '2019-20'
    mssql_sqlalchemy_connect_string = 'mssql+pyodbc://PHX-SQL07\BBOPS/BOps_DW?driver=SQL Server'
    mssql_engine = sqlalchemy.create_engine(mssql_sqlalchemy_connect_string)
    ingest_google_sheet_by_name(sheet_path, table_name, season, mssql_engine)
    generate_new_sheet_from_g_league_table_data(season, mssql_engine)


if __name__ == '__main__':
    main()
